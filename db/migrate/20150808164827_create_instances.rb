class CreateInstances < ActiveRecord::Migration
  def change
    create_table :instances do |t|
      t.string :aws_id
      t.string :status
      t.string :msg
      t.string :location
      t.string :access_key
      t.string :secret_key

      t.timestamps null: false
    end
  end
end
