//
// This module checks the state of a VM until it is up and running
// At that point it shows the location of the running VM so the user can
// go the application.
//

var StateChecker = function(opts) {
  // Constants and shared properties
  this.retryInterval = 2000;

  // The instance being launched state
  this.vmid = opts.id;
  this.initState = opts.status;
  this.status = opts.status;
  this.running = true;

  // Start checking
  this.check();
}

StateChecker.prototype = {
  check: function () {
    var that = this;
    
    if (this.status == 'running' ||
        this.status == 'error' ||
        this.status == 'terminated') {
      this.running = false;
    }

    if (this.running) {
      $.ajax({
        url: '/instances/' + this.vmid,
        dataType: 'script'
      }).done(function (data) {
        setTimeout(function () {
          that.check();
        }, that.retryInterval);
      });
    }
  }
}