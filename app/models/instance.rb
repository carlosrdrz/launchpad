class Instance < ActiveRecord::Base
  # Lets just add a small set of validations, although you would probably
  # need to strongly verify the instances field on a production environment
  validates :access_key, presence: true
  validates :secret_key, presence: true

  after_initialize :defaults

  private

  def defaults
    self.aws_id     ||= 'Not available yet'
    self.status     ||= :initializing
    self.msg        ||= 'Initializing'
    self.location   ||= 'Not available yet'
  end
end
