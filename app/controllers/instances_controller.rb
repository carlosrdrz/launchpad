class InstancesController < ApplicationController
  before_action :set_instance, only: [:show, :destroy]

  respond_to :html
  respond_to :js, only: [:show]

  def new
    @instance = Instance.new
  end

  def create
    @instance = Instance.new(instance_params)
    LaunchInstanceJob.perform_later @instance if @instance.save
    respond_with @instance
  end

  def show
    respond_with @instance
  end

  def destroy
    if @instance.status.to_sym == :running
      @instance.location = 'Not available'
      @instance.status = :destroying
      @instance.msg = 'Destroying instance'
      @instance.save
      DeleteInstanceJob.perform_later @instance
    end
  end

  private

  def set_instance
    @instance = Instance.find_by(id: params[:id])
  end

  def instance_params
    params.require(:instance)
      .permit(:access_key, :secret_key)
  end
end
