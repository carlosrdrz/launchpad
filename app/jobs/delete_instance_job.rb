class DeleteInstanceJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    # Parameters passed when the job is created
    instance = args[0]
    access_key = instance.access_key
    secret_key = instance.secret_key

    # Logger will log output to log/dj.log
    logger = Delayed::Worker.logger
    logger.debug "Instance #{instance.id} destroy started"

    begin
      # Create the client and resource with the user API KEY and SECRET KEY
      ec2 = Aws::EC2::Client.new(
        region: 'eu-central-1',
        credentials: Aws::Credentials.new(access_key, secret_key)
      )

      resource = Aws::EC2::Resource.new(client: ec2)
        
      # Destroy the instance
      logger.debug "Instance #{instance.id} destroying instance"
      ec2instance = resource.instance(instance.aws_id)
      ec2instance.terminate

      # Wait until the instance is destroyed
      logger.debug "Instance #{instance.id} waiting until terminated"
      ec2instance.wait_until_terminated

      # At this point the instance is terminated!
      logger.debug "Instance #{instance.id} terminated"
      instance.status = :terminated
      instance.msg = 'Terminated'
      instance.save

      logger.debug "Instance #{instance.id} destroy done!"
    rescue Aws::EC2::Errors::AuthFailure, Aws::Errors::MissingCredentialsError
      logger.error "Instance #{instance.id} destroy invalid credentials. Exiting..."
      instance.status = :error
      instance.msg = 'Invalid AWS credentials.'
      instance.save
      return
    end
  end
end
