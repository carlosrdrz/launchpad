class LaunchInstanceJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    # Parameters passed when the job is created
    instance = args[0]
    access_key = instance.access_key
    secret_key = instance.secret_key

    # Logger will log output to log/dj.log
    logger = Delayed::Worker.logger
    logger.debug "Instance #{instance.id} creating started"

    # Create the client and resource with the user API KEY and SECRET KEY
    ec2 = Aws::EC2::Client.new(
      region: 'eu-central-1',
      credentials: Aws::Credentials.new(access_key, secret_key)
    )

    resource = Aws::EC2::Resource.new(client: ec2)
      
    # Create a security group to allow port 80
    begin
      logger.debug "Instance #{instance.id} creating security group"
      instance.status = :creating_security_group
      instance.msg = 'Setting up'
      instance.save

      security_group = resource.create_security_group(
        group_name: 'Inbound80Allowed',
        description: 'Default security group with port 80 allowed'
      )

      security_group.authorize_ingress(
        from_port: 80,
        to_port: 80,
        ip_protocol: 'tcp',
        cidr_ip: '0.0.0.0/0'
      )
    rescue Aws::EC2::Errors::InvalidGroupDuplicate
      logger.debug "Instance #{instance.id} security group already exists"
    rescue Aws::EC2::Errors::AuthFailure, Aws::Errors::MissingCredentialsError
      logger.error "Instance #{instance.id} invalid credentials. Exiting..."
      instance.status = :error
      instance.msg = 'Invalid AWS credentials.'
      instance.save
      return
    end

    # Launch the instance
    instance.status = :creating_instance
    instance.msg = 'Creating instance'
    instance.save

    logger.debug "Instance #{instance.id} creating instance"
    ec2instance = resource.create_instances(
      # Use bitnami-wordpress-4.2.4-0-linux-ubuntu-14.04.1-x86_64-hvm-ebs
      image_id: 'ami-f6d2d6eb',
      min_count: 1,
      max_count: 1,
      instance_type: 't2.micro',
      security_groups: ['Inbound80Allowed']
    )

    # Wait until the instance has launched
    logger.debug "Instance #{instance.id} waiting until running"
    ec2instance.first.wait_until_running

    # At this point the instance is running.
    # Lets get the hostname and instance id
    instance.status = :describing
    instance.aws_id = ec2instance.first.id
    instance.msg = 'Fetching public DNS name'
    instance.save

    logger.debug "Instance #{instance.id} describing..."
    desc = ec2.describe_instances(instance_ids: [instance.aws_id]).reservations.first.instances.first
    instance.location = desc['public_dns_name']
    instance.aws_id = desc['instance_id']

    # Lets wait until the app is up and ready
    logger.debug "Instance #{instance.id} starting app..."
    instance.status = :starting
    instance.aws_id = ec2instance.first.id
    instance.msg = 'Starting application'
    instance.save

    tries = 0
    begin
      Net::HTTP.get(instance.location, '/')
    rescue Errno::ETIMEDOUT, Errno::ECONNREFUSED
      tries += 1
      logger.debug "Instance #{instance.id} timeout or conn refused..."
      sleep 15
      retry if tries < 10
    end

    instance.status = :running
    instance.msg = 'Running'
    instance.save

    logger.debug "Instance #{instance.id} done!"
  end
end
