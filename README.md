# Wordpress Launchpad

This RoR app is an example of a Launchpad that launches an instance of the
Bitnami Wordpress AMI on AWS.

The home page asks the user for its AWS Access Key and Secrey Key. After they
are sent, the backend creates a background job that creates an instance
of the Bitnami Wordpress AMI bitnami-wordpress-4.2.4-0-linux-ubuntu-14.04.1-x86_64-hvm-ebs.

The user will see how the instance is being created. After a while, the app
gets the instance domain name and lets the user launch the app with just a
click.

# How it works

This example has just one model: the instance. The instance keeps
information about the state of a request sent by an user. Mainly, it
keeps its state (running, terminated, creating...), its AWS instance id, its
public dns name and its credentials (access key and secret key).

When a user requests the creation of a new instance, or the deleting of an
instance, a job is created. A job is just a background task that will be executed
in the future.

This example uses a architecture of producer / consumers. The web server
produces jobs on a queue in a database, and a set of consumers fetch those
jobs and execute them.

In Rails, the jobs interface is called ActiveJobs, which provides an interface
for creating, deleting, executing jobs and so on. Then, the job executors,
which are implemented as gems, fetch those jobs and execute them. The job executor
used in this example is called DelayedJob.

Currently, there are two jobs: LaunchInstance and DeleteInstance. Both are
background jobs that make API calls to the AWS endpoint.

When the jobs are executed, these jobs perform changes on the AWS account and
modifies the state of instances (so the user gets notified).

There is a log filed at log/dj.log that logs the state of the job workers.

While the job is running, the client code (javascript) continuosly requests the
state of the instance being created/delated, so the user always have in-time
information about what is going on with the instance.

# Installing ruby, rails and gems

First, you will need Ruby & Ruby on Rails.
The fastest way to install them is using RVM

```console
$ \curl -sSL https://get.rvm.io | bash -s stable --rails
```

After that, you just need to use bundler the fetch the app gems

```console
$ bundle install
```

# Running on development

For a development environment we will use one machine with a sqlite database.
The app is already configured to use sqlite, so you just need to create the
database and apply the database migrations.

```console
$ rake db:create
$ rake db:migrate
```

Then, you'll need to start the worker daemon to allow the creation and deletion
of virtual machines.

```console
$ ./bin/delayed_job start
```

You can stop the worker later with the following command.

```console
./bin/delayed_job stop
```

After that, just execute the web server.

```console
$ rails s
```

Now, you would be able to access the web app at
[http://localhost:3000/](http://localhost:3000/).

# Running on production

For a production environment, you will need a set of servers to execute the
web server, the database, and the set of workers. I didn't actually tried to
execute this example in a production-like environment, but to do so you would
need to follow the following steps:

## Configure the database

You will need to set up a database server. After that, modify the file
at config/database.yml to include the database credentials on the production
section.

## Create the database and run the migrations

The following commands will create the database if it does not exists, and also
run the migrations needed to store the models on that database.

```console
$ rake db:create
$ rake db:migrate
```

## Compile the assets

The following command joins and minifies the contents of javascript and css
files in two unique files: one for javascript, and one for css. Doing this, an 
user just needs to download to files to access the whole app.

```console
$ rake assets:precompile
```

## Run the server on production

This example uses WEBrick, but probably Puma or Unicorn would be better
choices for a production environment.

```console
$ RAILS_ENV=production rails s
```

## Run the job workers

Get a few nodes to run the creation/deletion jobs. To do that just install
the app on a new node, and run one of the following commands to start the
job daemon:

```console
$ RAILS_ENV=production ./bin/delayed_job start
```

You can stop the worker later with the following command.

```console
$ RAILS_ENV=production ./bin/delayed_job stop
```

# Gems

In addition to the gems bundled with Rails, I've used the following gems:

## delayed_job_active_record and daemons
They provide the functionality of background jobs and workers

## aws-sdk
Used to call the AWS API in order to create/delete instances

## simple_form and responders
Just utilities to create forms easily and to being able to specify the
http entities accepted in each request.

# Notes

I think that at this point the app is pretty scalable. The consumer / producer
schema for the jobs works pretty well and could be used to scale the number
of concurrent VMs creations.

From a security point of view, this example is far from perfect. The access key
and secret keys are both stored on the database as clear text, meaning that if
an attacker get access to the server, it gets access to all the keys.

Of course, a production environment wouldn't implement something like this, but
since this is just a small and fast example, I thought this was the fastest
way of implementing the launchpad.
